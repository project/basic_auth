# Basic HTTP Authentication

This module provides a possibility to restrict access to specific paths using
basic HTTP authentication, in addition to standard Drupal access checks. Paths
can be configured through the UI or programmatically.

To expand on "in addition to": the authentication check is performed (and is
configured) independently from any other standard Drupal access checks.
This means
- If a regular non-logged-in visitor visits a URL that is protected using Basic
  HTTP authentication and enters the correct user / password combination, they
  will still get Drupal 'access denied' page if the path is not accessible to
  anonymous users.
- If a user is logged in through Drupal, they will still get the password
  prompt when they visit a protected URL. After they enter the correct user /
  password combination, they will only be able to see the page if their Drupal
  user has access to do so.

Version 1.x of this module can only protect paths as mentioned in hook_menu
entries (including any % signs).

## API

Add basic HTTP authorisation for a path:

```php
basic_auth_config_edit('admin/config', TRUE, 'admin', 'passw0rd');
```

Check, that HTTP authentication enabled for a path:

```php
basic_auth_config_exists('admin/config', TRUE) === TRUE;
```

Disable HTTP authentication for a path:

```php
basic_auth_config_edit('admin/config', FALSE);
```

Check, that config exists for a path:

```php
basic_auth_config_exists('admin/config');
```

## Todo

- (Optionally) store configuration/passwords in code instead of database
- Hash passwords
